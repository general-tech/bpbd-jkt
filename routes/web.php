<?php
use App\Http\Middleware\checkLogin;


Auth::routes();
Route::get('/', function () {
  return redirect('/home');
});

// home
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home/listmap', 'HomeController@listmap');

// cctv
Route::get('/cctv', 'CctvController@index')->name('cctv');
Route::get('/cctv/{detail}', 'CctvController@detail');
Route::get('/cctv/data/datajson', 'CctvController@dataJSON');
Route::post('/cctv/validasi', 'CctvController@validasi');

// analytics
Route::get('/analytics', 'AnalyticsController@index')->name('analytics');
Route::post('/analytics/dataChart_hari', 'AnalyticsController@dataChart_hari');
Route::post('/analytics/dataChart_jam', 'AnalyticsController@dataChart_jam');



// setting
Route::get('/setting', 'SettingController@index')->name('setting');

	// camera crud
	Route::get('/setting/camera', 'SettingController@indexCamera');
	Route::get('/setting/camera/create', 'SettingController@createCamera');
	Route::post('/setting/camera', 'SettingController@storeCamera');
	Route::get('/setting/camera/delete/{id}', 'SettingController@deleteCamera');
	Route::get('/setting/camera/edit/{id}', 'SettingController@editCamera');
	Route::get('/setting/camera/datajson', 'SettingController@dataJSON');

	Route::post('/setting/camera/update', 'SettingController@updateCamera');

	// user crud
	Route::get('/setting/user', 'UserController@index');
	Route::get('/setting/user/create', 'UserController@create');
	Route::post('/setting/user', 'UserController@store');
	Route::get('/setting/user/datajson', 'UserController@dataJSON');

	Route::get('/setting/user/delete/{id}', 'UserController@delete');
	Route::get('/setting/user/edit/{id}', 'UserController@edit');
	Route::post('/setting/user/update', 'UserController@update');
	
	
	// notifikasi
	Route::get('/getnotif', 'NotifController@index');
	Route::get('/notifikasi', 'NotifController@all');

