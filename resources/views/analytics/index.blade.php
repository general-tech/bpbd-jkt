@extends('layouts.app')
@section('content')
	<div class="content">
		<div class="col-xs-12">
			<div class="row">
				{{-- left sidebar --}}
				<div class="col-md-3 sidebar">
					<div class="col-xs-12 m-t-30">
						<div class="form-group">
							<label for="exampleInputEmail1">Type Detection</label>
							<select name="type" class="form-control" id="type">
								@foreach ($getList as $vlist)
									<option value="{{$vlist->token_detection}}">{{$vlist->type}}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">Waktu</label>
							<select  class="form-control" name="t_d" id="t_d">
								<option value="hari" rel="line">Perhari</option>
								<option value="jam" rel="bar">Perjam</option>
							</select>
						</div>
						
						<div class="row" id="_date">
							<div class="col-xs-6 ss_date">
								<div class="form-group">
									<label for="exampleInputEmail1">Dari Tanggal</label>
									<input type="text" class="form-control datepicker" name="s_date" id="s_date">
								</div>
							</div>
							<div class="col-xs-6 ee_date">
								<div class="form-group">
									<label for="exampleInputEmail1">Sampai Tanggal</label>
									<input type="text" class="form-control datepicker" name="e_date" id="e_date">
								</div>
							</div>
						</div>

						<div class="row" id="_time" style="display: none">
							<div class="col-xs-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Dari Waktu</label>
									<select  class="form-control" name="s_time" id="s_time">
										@for ($i = 0; $i < 25; $i++)
											@if ($i < 10)
												<option value="0{{$i}}:00">0{{$i}}:00</option>
												@else	
												<option value="{{$i}}:00">{{$i}}:00</option>
											@endif
										@endfor
									</select>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Sampai Waktu</label>
									<select  class="form-control" name="e_time" id="e_time">
										@for ($i = 0; $i < 25; $i++)
											@if ($i < 10)
												<option value="0{{$i}}:00">0{{$i}}:00</option>
												@else	
												<option value="{{$i}}:00">{{$i}}:00</option>
											@endif
										@endfor
									</select>
								</div>
							</div>
						</div>

						<div class="row mt-25">
							<div class="col-xs-12">
								<button class="btn btn-orange col-xs-12" onclick="send_data()">Kirim</button>
							</div>
						</div>

					</div>
				</div>
				{{-- end left sidebar --}}
					
				
				<div class="col-md-9">
					<div class="mt-25">
							<div class="col-xs-12">

								<div class="row title_chart" style="margin-bottom: 50px">
									
									{{-- <h1 class="col-xs-12 mb-25">Indetifikasi Banjir</h1>
									
									<div class="col-xs-12">
										<div class="fl-left" style="margin: 0px 20px 0 0">
											<b>ID Kamera:</b>
										</div>
										<a href="#" class="btn btn-success btn-small">
											51
										</a>
									</div> --}}
									

								</div>

							</div>
							<canvas id="myChart" width="100%" ></canvas>
					</div>
				</div>
				
			</div>
		</div>    
	</div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
	<script>
	  $(document).ready(function(){
	  	$( "#t_d" ).change(function(){
	  		if($("#t_d").val() == 'jam'){
					$('#_time').slideDown();

					$('.ss_date').removeClass('col-xs-6').addClass('col-xs-12');
					$('.ee_date').slideUp();

	  		} else {
	  			$('#_time').slideUp();

					$('.ss_date').removeClass('col-xs-12').addClass('col-xs-6');
	  			$('.ee_date').slideDown();
	  		}
	  	});
			
			var date = new Date();
	    $( ".datepicker" ).datepicker({
	        format: 'yyyy-mm-dd'
	    }).datepicker("setDate", date);
			

			
  		date.setDate(date.getDate()-1); 
	    $( "#s_date" ).datepicker({
	        format: 'yyyy-mm-dd'
	    }).datepicker("setDate", date);

	  })


	function send_data(){
		$.ajax({
	    type:'post',
	    url:'/analytics/dataChart_'+$('#t_d').val(),
	    data:{
		     "_token": "{{csrf_token()}}",
		     "type": $('#type').val(),
		     "t_d": $('#t_d').val(),
		     "s_date": $('#s_date').val(),
		     "e_date": $('#e_date').val(),
		     "s_time": $('#s_time').val(),
		     "e_time": $('#e_time').val(),
	    },
	    success: function(data){
	    	// console.log(data);
				if(data.dataAnalytic.length !== 0){
					

					var ctx = document.getElementById("myChart");

					var dataFix = [];
					var label = [];

					$.each(data.dataAnalytic, function(key, value) {
						var c_data = [];
						$.each(value, function(key, value2) {
							c_data.push(value2.c_data);
						});

						dataFix.push({
							'label': '#' + value[0].label + ' ',
							'data': c_data,
							'backgroundColor': value[0].color,
	            'borderColor': 'rgba(255,99,132,1)',
	            'borderWidth': 1
						});

						// button id kamera
						label.push(
							'<a href="/cctv/'+value[0].label+'" class="btn btn-success btn-small"  target="_blank">'+value[0].label+'</a>'
						);
					});
					
					$('.title_chart').html('<h2 class="col-xs-12 mb-25">'+data.label.type+'</h2><div class="col-xs-12"> <div class="fl-left" style="margin: 0px 20px 0 0"> <b>ID Kamera:</b> </div>'+label+'</div>'
					);
					
					// console.log(2, $('#t_d').find('option:selected').attr('rel'));
					var myChart = new Chart(ctx, {
					    // type: $('#t_d').find('option:selected').attr('rel'),
					    type: 'bar',
					    data: {
				        labels: data.c_date,
				        datasets: dataFix
					    }
					});
				} else {
					alert('Data Kosong');
				}
			}
		});
	}
	</script>

@endsection

  

