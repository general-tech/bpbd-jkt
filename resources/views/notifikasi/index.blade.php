@extends('layouts.app')
@section('content')

<div class="container p-b-60">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h2 class="text-center">Notifikasi</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7 col-md-offset-2">
            <div class="table-responsive m-b-20">
                <table class="table table-hover mails m-0">
                    <tbody>
                        @foreach ($getData as $data)
                            <tr>
                                <td class="mail-select" style="min-width: auto;">
                                    <i class="fa fa-circle m-l-5 text-warning"></i>
                                </td>

                                <td>
                                    <a href="email-read.html" class="email-name">#{{$data->id_camera}}</a>
                                </td>
                                <td class="hidden-xs">
                                    <a href="email-read.html" class="email-msg">{{$data->type}}</a>
                                </td>
                                <td>
                                    {{date('D, d M Y', strtotime($data->date))}}
                                </td>

                                <td class="text-right" style="width: 200px">
                                    {{$data->time}} Wib
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
            <div class="text-center m-t-30">
                {{ $getData->links() }}
            </div>
        </div>
    </div>

</div>


@endsection