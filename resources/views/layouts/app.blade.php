<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>{{ config('app.name', 'BPBD DKI Jakarta') }}</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex">
        <link rel="shortcut icon" href="{{asset('img/logo.png')}}" type="image/x-icon"/> 
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- App css -->
        <link href="{{ asset('themes/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('themes/assets/css/core.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('themes/assets/css/components.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('themes/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('themes/assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('themes/assets/css/menu.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('themes/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ asset('themes/plugins/switchery/switchery.min.css') }}">
        <script src="{{ asset('themes/assets/js/modernizr.min.js') }}"></script>
        {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

    </head>


    <body>


        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container">

                    <div class="logo">
                      <a href="{{url('/')}}">
                        <div class="img">
                            <img src="{{asset('img/logo.png')}}">
                        </div>
                        <div class="text">
                            <b>BPBD</b> <br>
                            DKI Jakarta
                        </div>
                      </a>
                    </div>


                    <div class="menu-extras">

                        <ul class="nav navbar-nav navbar-right pull-right">

                           {{--  <li class="navbar-c-items">
                                <form role="search" class="navbar-left app-search pull-left hidden-xs">
                                     <input type="text" placeholder="Search..." class="form-control">
                                     <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li> --}}

                            <li class="dropdown navbar-c-items">
                                <a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">
                                    <i class="mdi mdi-bell"></i>
                                    {{-- <span class="badge up bg-success">4</span> --}}
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-lg user-list notify-list">
                                    <li class="text-center">
                                        <h5>Pemberitahuan</h5>
                                    </li>
                                    @foreach (DB::table('report')->join('type_detections', 'report.type_detection', '=', 'type_detections.id_detection')->orderBy('id',' desc')->limit(5)->get() as $reports)
                                        <li>
                                            <a href="{{ url('/cctv/'.$reports->id_camera) }}" class="user-list-item">
                                                <div class="icon bg-info">
                                                    <i class="ti-video-camera"></i>
                                                </div>
                                                <div class="user-desc">
                                                    <span class="name">{{$reports->type}}</span>
                                                    <span class="time">{{$reports->updated_at}}</span>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                   
                                    <li class="all-msgs text-center">
                                        <p class="m-0"><a href="{{url('/notifikasi')}}">Lihat Semua</a></p>
                                    </li>
                                </ul>
                            </li>

                            <li class="dropdown navbar-c-items">
                                <a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown" aria-expanded="true">
                                    <img src="{{asset('img/logo.png')}}" alt="user-img" class="img-circle"> 
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                                    <li class="text-center">
                                        <h5>Hi, {{ \Auth::user()->name }}</h5>
                                    </li>
                                    <li><a href="{{route('setting')}}"><i class="ti-settings m-r-5"></i> Pengaturan</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="ti-power-off m-r-5"></i> Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="menu-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </div>
                    </div>
                    <!-- end menu-extras -->

                </div> <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                            <li><a href="{{ route('home') }}"><i class="mdi mdi-view-dashboard"></i> Dashboard</a></li>
                            
                            <li><a href="{{ route('cctv') }}"><i class="ti-video-camera"></i> Kamera CCTV</a></li>
                            <li><a href="{{ route('analytics') }}"><i class="mdi mdi-chart-bar"></i> Data Analitik</a></li>
                            <li><a href="{{route('setting')}}"><i class="ti-settings m-r-5"></i> Pengaturan</a></li>
                        </ul>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div>
             <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="">

               
                @yield('content')

                <!-- Footer -->
                {{-- <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                &copy; 2017 BPBD DKI Jakarta
                            </div>
                        </div>
                    </div>
                </footer> --}}
                <!-- End Footer -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        
        <!-- jQuery  -->
        <script src="{{ asset('themes/assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('themes/assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('themes/assets/js/detect.js') }}"></script>
        <script src="{{ asset('themes/assets/js/fastclick.js') }}"></script>
        <script src="{{ asset('themes/assets/js/jquery.blockUI.js') }}"></script>
        {{-- <script src="{{ asset('themes/assets/js/waves.js') }}"></script> --}}
        <script src="{{ asset('themes/assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('themes/assets/js/jquery.scrollTo.min.js') }}"></script>
        <script src="{{ asset('themes/plugins/switchery/switchery.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('themes/assets/js/jquery.core.js') }}"></script>
        <script src="{{ asset('themes/assets/js/jquery.app.js') }}"></script>
        
        <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
        <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('js/bootstrap-datepicker-1-6-4.min.js') }}"></script>
        <script src="{{ asset('js/notif.js') }}"></script>
        @yield('script')
        

    </body>
</html>