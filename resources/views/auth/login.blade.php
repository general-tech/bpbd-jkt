@extends('layouts.appLogin')
@section('content')
  <div class="auth">
    <div class="col-md-4 col-md-offset-4">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="auth__logo">
            <img src="/img/logo.png" alt="speed camera korlantas polri">
          </div>
          <div class="auth__title">BPBD <span>Provinsi DKI Jakarta</span></div>
          <form name='form-login' method="POST" action="{{ route('login') }}" autocomplete="off">
            {{ csrf_field() }}
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
              <input type="email" class="form-control" placeholder="Email" name="email">
              @if ($errors->has('email'))
              <p class="error">{{ $errors->first('email') }}</p>
              @endif
            </div>
            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
              <input type="password" class="form-control" placeholder="Password" name="password">
              @if ($errors->has('password'))
              <p class="error">{{ $errors->first('password') }}</p>
              @endif
            </div>
            <button type="submit" class="btn btn-success col-xs-12">
              Masuk
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
