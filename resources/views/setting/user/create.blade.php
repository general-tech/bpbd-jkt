@extends('setting.layout')

@section('contents')
    <div class="panel panel-color panel-orange">
        <div class="panel-heading">
            <h3 class="panel-title">Tambah Baru Akun</h3>
        </div>

        <div class="panel-body">
                <form method="POST" action="{{ url('setting/user') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Nama lengkap: <span class="text-danger">*</span></label>
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                        <div>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Email: <span class="text-danger">*</span></label>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                        <div class="col-md-6">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Password: <span class="text-danger">*</span></label>
                        <input id="password" type="password" class="form-control" name="password" required>
                        <div class="col-md-6">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm">Confirm Password: <span class="text-danger">*</span></label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>

                     <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                        <label for="password-confirm">Jabatan: <span class="text-danger">*</span></label>
                        <select name="role" class="form-control" required>
                            <option value="Admin">Admin</option>
                            <option value="Visitor">Visitor</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-orange">
                            Daftar
                        </button>
                    </div>
                </form>
        </div>
    </div>
        
@endsection
