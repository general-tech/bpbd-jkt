@extends('setting.layout') 
@section('contents')
    <div class="panel panel-color panel-orange">
        <div class="panel-heading">
            <h3 class="panel-title">Semua Akun</h3>
        </div>
        <div class="panel-body">
            <table id="example" class="display table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                    <th width="20">No</th>
                    <th width="500">Nama</th>
                    <th width="300">Email</th>
                    <th>Role</th>
                    @if (Auth::user()->role == 'Admin')
                        <th></th>
                    @endif
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Role</th>
                    @if (Auth::user()->role == 'Admin')
                        <th></th>
                    @endif
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>


@endsection 

@section('script')
<link rel="stylesheet" href="{{asset('js/jquery.dataTables.min.css')}}">
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "ajax": "/setting/user/datajson"
        });
    });
</script>
@endsection