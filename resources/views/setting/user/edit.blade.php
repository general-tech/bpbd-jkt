@extends('setting.layout')

@section('contents')
	<div class="panel panel-color panel-orange">
		<div class="panel-heading">
			<h3 class="panel-title">Ubah Akun</h3>
		</div>
		<div class="panel-body">
			<form action="{{ url('/setting/user/update') }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="id" value="{{ $users->id }}">
				<div class="form-group">
						<label for="email">Nama Lengkap:</label>
						<input type="text" class="form-control" name="name" value="{{ $users->name }}">
					</div>
					<div class="form-group">
						<label for="email">Email :</label>
						<input type="text" class="form-control" name="email" value="{{ $users->email }}">
					</div>
					<div class="form-group">
						<label for="pwd">Jabatan:</label>
						<select name="role" class="form-control" required>
								<option value="{{ $users->role }}">{{ $users->role }}</option>
								<option value="Admin">Admin</option>
								<option value="Visitor">Visitor</option>
						</select>
					</div>
					<button type="submit" class="btn btn-orange">Ubah</button>
			</form>
		</div>
	</div>
@endsection