@extends('layouts.app')
@section('content')
    <div class="content mt-25">
      <div class="container_10">
        <div class="row">
            <div class="col-sm-2 col-md-2">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Kamera</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <a href="{{ url('setting/camera') }}" class="text-orange">Lihat Semua</span>
                                        </td>
                                    </tr>
                                    @if (Auth::user()->role == 'Admin')
                                        <tr>
                                            <td>
                                                <a href="{{ url('setting/camera/create') }}" class="text-orange">Tambah Baru</a>
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Akun</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <a href="{{url('setting/user')}}" class="text-orange">Lihat Semua</a>
                                        </td>
                                    </tr>
                                    @if (Auth::user()->role == 'Admin')
                                        <tr>
                                            <td>
                                                <a href="{{url('setting/user/create')}}" class="text-orange">Tambah Baru</a>
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="col-sm-10 col-md-10">
                @yield('contents')
            </div>
        </div>
      </div>
    </div>


@endsection
