@extends('setting.layout')

@section('contents')
    <div class="panel panel-color panel-orange">
        <div class="panel-heading">
            <h3 class="panel-title">Semua Kamera</h3>
        </div>
        <div class="panel-body">
                <table id="example" class="display table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kota</th>
                            <th>Kecamatan</th>
                            <th>Kelurahan</th>
                            <th width="50">URL</th>
                            <th>Tanggal</th>
                            @if (Auth::user()->role == 'Admin')
                                <th width="200" align="text-center"></th>
                            @endif              
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Kota</th>
                            <th>Kecamatan</th>
                            <th>Kelurahan</th>
                            <th>URL</th>
                            <th>Tanggal</th>
                            @if (Auth::user()->role == 'Admin')
                                <th></th>
                            @endif
                            
                        </tr>
                    </tfoot>
                </table>
            </div>
    </div>

@endsection

@section('script')
	<link rel="stylesheet" href="{{asset('js/jquery.dataTables.min.css')}}">
	<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
	<script>
		$(document).ready(function() {
		    $('#example').DataTable( {
		        "ajax": "/setting/camera/datajson"
		    });
		});
	</script>
@endsection