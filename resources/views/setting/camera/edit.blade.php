@extends('setting.layout')

@section('contents')
<div class="panel panel-color panel-orange">
		<div class="panel-heading">
				<h3 class="panel-title">Ubah Kamera</h3>
		</div>
		<div class="panel-body">
			<form action="{{ url('/setting/camera/update') }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="id" value="{{ $camera->id }}">
				<div class="form-group">
						<label for="email">ID Kamera: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="id_camera" value="{{ $camera->id_camera }}" required>
					</div>
					<div class="form-group">
						<label for="email">Kota: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="kota" value="{{ $camera->kota }}" required>
					</div>
					<div class="form-group">
						<label for="pwd">Kecamatan: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="kecamatan" value="{{ $camera->kecamatan }}" required>
					</div>
					<div class="form-group">
						<label for="email">Kelurahan: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="kelurahan" value="{{ $camera->kelurahan }}" required>
					</div>
					<div class="form-group">
						<label for="pwd">Latitude: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="lat" value="{{ $camera->lat }}" required>
					</div>
					<div class="form-group">
						<label for="email">Longitude: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="lng" value="{{ $camera->lng }}" required>
					</div>
					<div class="form-group">
						<label for="pwd">URL: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="url" value="{{ $camera->url }}" required>
					</div>
					<button type="submit" class="btn btn-orange">Submit</button>
			</form>
		</div>

</div>
@endsection