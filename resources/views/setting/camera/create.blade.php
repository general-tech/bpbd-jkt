@extends('setting.layout')

@section('contents')
	<div class="panel panel-color panel-orange">
		<div class="panel-heading">
			<h3 class="panel-title">Tambah Baru Kamera</h3>
		</div>

		<div class="panel-body">
			<form action="{{ url('/setting/camera') }}" method="POST">
				{{ csrf_field() }}
				<div class="form-group">
						<label for="email">ID Kamera: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="id_camera" required>
					</div>
					<div class="form-group">
						<label for="email">Kota: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="kota" required>
					</div>
					<div class="form-group">
						<label for="pwd">Kecamatan: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="kecamatan" required>
					</div>
					<div class="form-group">
						<label for="email">Kelurahan: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="kelurahan" required>
					</div>
					<div class="form-group">
						<label for="pwd">Latitude: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="lat" required>
					</div>
					<div class="form-group">
						<label for="email">Longitude: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="lng" required>
					</div>
					<div class="form-group">
						<label for="pwd">URL: <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="url" required	>
					</div>
					<button type="submit" class="btn btn-orange">Kirim</button>
			</form>
		</div>
	</div>
@endsection