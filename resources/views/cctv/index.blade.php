@extends('layouts.app')
@section('content')
  <div class="container p-b-60">

      <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                
                  <h4 class="page-title">Kamera CCTV</h4>
              </div>
          </div>
      </div>

      <div class="row">
      		@foreach ($cctv_sampah as $cctv_s)
	          <div class="col-md-4 col-sm-6">
	              <div class="property-card">
	                  <div class="property-image">
	                  		<iframe src="{{$cctv_s->url}}" frameborder="0" width="100%" height="100%"></iframe>
	                      	<span class="property-label label label-orange">#{{$cctv_s->id_camera}}</span>
	                  </div>

	                  <div class="property-content">
	                      <div class="listingInfo">
	                          <div class="">
	                              <h5 class="text-orange m-t-0">{{$cctv_s->type}}</h5>
	                          </div>
	                          <div class="">
	                              <h3 class="text-overflow">
	                              	<a href="#" class="text-dark">Kamera {{$cctv_s->id_camera}} | {{$cctv_s->type}}</a>
	                             </h3>
	                              <p class="text-muted text-overflow"><i class="mdi mdi-map-marker-radius m-r-5"></i>{{$cctv_s->kelurahan}} {{$cctv_s->kota}}, DKI Jakrta</p>

	                              <div class="m-t-20">
	                                  <a href="{{url('cctv/'.$cctv_s->id_camera)}}" class="btn btn-orange btn-block waves-effect waves-light">Lihat Selengkapnya</a>
	                              </div>

	                          </div>
	                      </div>

	                  </div>

	              </div>

	          </div>
	        @endforeach
      </div>

  </div>

@endsection
