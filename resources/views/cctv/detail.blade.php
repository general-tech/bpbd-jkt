@extends('layouts.app')
@section('content')
    <div class="container p-b-60">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">    
                  <h4 class="page-title">Detail Kamera <b>(#{{$cameras->id_camera}} - {{$cameras->type}})</b></h4>
                </div>
            </div>
        </div>
        

        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    
                    <div class="col-lg-3 col-md-4">
                        <div class="text-center card-box">
                            <div class="member-card">

                                <iframe src="{{$cameras->url}}" frameborder="0" width="100%" height="300"></iframe>
                                
                                <div class="m-b-20"></div>

                                <div class="text-left">
                                    <p class="text-muted font-13"><strong>ID Kamera :</strong> <span class="m-l-15">{{$cameras->id_camera}}</span></p>
                                    <p class="text-muted font-13"><strong>Tipe Deteksi :</strong> <span class="m-l-15">{{$cameras->type}}</span></p>
                                    <p class="text-muted font-13"><strong>Kota :</strong> <span class="m-l-15">{{$cameras->kota}}</span></p>
                                    <p class="text-muted font-13"><strong>Kecamatan :</strong> <span class="m-l-15">{{$cameras->kecamatan}}</span></p>
                                    <p class="text-muted font-13"><strong>Kelurahan :</strong> <span class="m-l-15">{{$cameras->kelurahan}}</span></p>
                                </div>
                            </div> <!-- end member-card -->
                        </div> <!-- end card-box -->
                    </div> <!-- end col -->



                    <div class="col-md-8 col-lg-9">
                        
                        <div class="row">
                            
                            <div class="card-box">
                                <div class="member-card">
                                    <div id="_map" style="height: 300px; display: inline-block; width: 100%"></div>
                                </div>
                            </div>
                            <div class="card-box">
                                <div class="member-card">
                                    <table id="example" class="display table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th width="20">No</th>
                                              <th>Tanngal</th>
                                              <th>Jam</th>
                                              <th>Skor</th>
                                              <th>Deskripsi</th>
                                              <th width="100">Gambar</th>
                                              <th width="100">Status</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                              <th width="20">No</th>
                                              <th>Tanngal</th>
                                              <th>Jam</th>
                                              <th>Skor</th>
                                              <th>Deskripsi</th>
                                              <th width="100">Gambar</th>
                                              <th width="100">Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <!-- end col -->

                           

                        </div> <!--end row -->
                    </div>

                </div>
            </div>
        </div>
         

  </div>
    
@endsection

@section('script')
    <link rel="stylesheet" href="{{asset('js/jquery.dataTables.min.css')}}">
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2v1bPZtRoHDjaFeF9dkjDCACwyHoREFM&libraries=places&callback=initMap" async defer></script>

    <script>
        $(document).ready(function() {
          $('#example').DataTable({
              "ajax": "/cctv/data/datajson?id=" + {{$cameras->id_camera}}
          });
        });

        function valid(indicator, id){
            $.ajax({
                type:'POST',
                url:'/cctv/validasi',
                data:{
                    "_token": "{{csrf_token()}}",
                    "indicator": indicator,
                    "id": id
                }, 
                beforeSend: function() {
                    $('.btn_valid_'+id).html('Loading...');
                },
                success:function(res){
                    console.log('res');
                    if (res.status) {
                        $('.btn_valid_'+id).html(res.type);
                    }else{
                        alert('Data tidak ditemukan!!');
                    }
                }
            });
        }

        function initMap() {
            var location = {lat: {{$cameras->lat}}, lng: {{$cameras->lng}}}
            var map = new google.maps.Map(document.getElementById('_map'), {
              center: location,
              zoom: 16,
            });
                
            var marker = new google.maps.Marker({
              position: location,
              icon: '../img/ic-cctv.png',
              map: map,
            });
            marker.setMap(map);
        }
    </script>

@endsection