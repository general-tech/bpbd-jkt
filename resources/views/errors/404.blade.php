@extends('layouts.app')
@section('content')
    

     <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">

                <div class="wrapper-page">
                    <img src="{{ asset('themes/assets/images/animat-search-color.gif') }}" alt="" height="120">
                    <h2 class="text-uppercase text-danger">Page Not Found</h2>
                    

                    <a class="btn btn-success waves-effect waves-light m-t-20" href="{{ url('/') }}"> Return Home</a>
                </div>

            </div>
        </div>
    </div>


@endsection
