@extends('master')
@section('content')


    <div class="page {{$class}}">
        <div class="container">
            <div class="img full">
                <div class="centered">
                    <img src="{{URL::to('/assets/img/bg-503.png')}}">
                </div>
            </div>
            <div class="txt full">
                <div>SITUS SEDANG DALAM PERBAIKAN</div>
                <p>
                    Saat ini server Geevv sedang dalam perbaikan...
                    <br />
                    Terima kasih atas kesabarannya.
                </p>
            </div>
            <div class="btn full">
                <a href="javascript:location.reload()">
                    Muat ulang
                </a>
            </div>
        </div>
    </div>

    @include('pages.footer_new')

@stop