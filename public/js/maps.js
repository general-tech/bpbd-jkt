	var map;
  var markers = [];
  function initMap() {
    map = new google.maps.Map(document.getElementById('_map'), {
      center: {
        lat: -6.211800, 
        lng: 106.819965
      },
      zoom: 13,
    });
    autocomplete(map);

  }

  function autocomplete(map){

    var input = document.getElementById('pac-input');
    var jakarta = new google.maps.LatLng( -6.229728, 106.6894312);
    var hyderabadBounds = new google.maps.LatLngBounds( jakarta );

    var autocomplete = new google.maps.places.Autocomplete((input), {
      // types: ['(cities)'],
      bounds: hyderabadBounds,
      componentRestrictions: {'country': 'id'}
    });    

    autocomplete.addListener('place_changed', function() {
      // console.log(markers.length);
      if(markers.length > 0){
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(null);
        }
      }
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
      map.setCenter(place.geometry.location);
      map.setZoom(16);

      $.ajax({
         type:'POST',
         url:'/home/listmap',
         data:{
          "_token": "{{csrf_token()}}",
          "lat": place.geometry.location.lat(),
          "lng": place.geometry.location.lng()
        },
         success:function(data){
            for (var i = 0; i < data.length; i++) {

              var marker = new google.maps.Marker({
                position: {
                  lat: data[i].lat, 
                  lng: data[i].lng,
                },
                icon: './img/ic-cctv.png',
                map: map,
              });
              markers.push(marker);

              // var infowindow = new google.maps.InfoWindow();
              // var content = '<iframe src='+data[i]+'frameborder="0" width="50" height="50"></iframe>';
              // google.maps.event.addListener(marker,'click', (function(marker, content, infowindow){ 
              //   return function() {
              //      infowindow.setContent(content.url);
              //      infowindow.open(map,marker);
              //   };
              // })(marker, content, infowindow)); 

            };

            if(data.length > 0){
              $('.list_marker .list_ ul').html(' ');
              $.each(data, function(key, value) {
                $('.list_marker').css({'left': 0});
                $('.list_marker a.ic').html('<');
                var listMaps = '<li key="'+key+'"> <div class="vid"> <iframe src="'+value.url+'" frameborder="0" width="100%"></iframe> </div> <div class="desc_"> <div><b>Kota:</b> '+value.kota+'</div> <div><b>Kecamatan:</b> '+value.kecamatan+'</div> <div><b>Kelurahan:</b> '+value.kelurahan+'</div> <div class="readmore"> <div><a href="javascript:;" onclick="fokus('+value.lat+', '+value.lng+')"> Lihat Marker </a></div> <div><a href="'+value.url+'" target="_blank">Lihat Kamera</a></div> </div> </div> </li>';
                $('.list_marker .list_ ul').append(listMaps);
                $('.notfound_').remove();
              });
            } else {
              $('.list_marker .list_').append('<div class="notfound_">Data Kosong</div>');
              $('.list_marker .list_ ul').html(' ');
            }
         }
      });
    });
  }

  function fokus(lat, lng){
    map.setCenter(new google.maps.LatLng(lat, lng));
    map.setZoom(18);
  }
  
  function accor(){
    if($('.list_marker').css('left') == '0px'){
      $('.list_marker').css({
        'left': '-25%'
      });
      $('.list_marker a.ic').html('>');
    }else{
      $('.list_marker').css({
        'left': 0
      });
      $('.list_marker a.ic').html('<');
    }
    
  } 