#include <mysql.h>
#include <stdio.h>
#include <iostream>

class DB {
  private:
    MYSQL *con;
    std::string table;
    std::string column;
    std::string value;
    std::string name;


  public:
    DB();
    ~DB();

    bool init(
        std::string host, 
        std::string username, 
        std::string password, 
        std::string database, 
        std::string table, 
        std::string column, 
        std::string name
        );
    bool insert(float number);
    bool insert(float number, char filename[]);
};
