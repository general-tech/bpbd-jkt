import face_recognition
import cv2
import numpy
import mysql.connector
import threading
import time
import os
import re
import sys

age_net = cv2.dnn.readNetFromCaffe("./age_net_definitions/deploy.prototxt", "./models/age_net.caffemodel")

gender_net = cv2.dnn.readNetFromCaffe("./gender_net_definitions/deploy.prototxt", "./models/gender_net.caffemodel")

global_rect = (0,0,0,0)
global_name = ''
global_gui = False
global_stream = False


class FaceRecognizer(threading.Thread):

    def __init__(self, connection, dataName, dataEncoding, frame):
        threading.Thread.__init__(self)
        self.dataName = dataName
        self.dataEncoding = dataEncoding
        self.frame = frame
        self.connection = connection

    def run(self):
        global global_name
        global global_rect
        rects = face_recognition.face_locations(self.frame, 1)
        if len(rects) > 0:
            item = face_recognition.face_encodings(self.frame, rects)[0]
            result = self._knn(item)
            global_rect = rects[0]
            if result == None:
                age, gender, filename = self._detectAgeAndGender(rects[0])
                global_name = gender + " " + age
                self._insertdb(gender + " " + age, filename)
            else:
                global_name = result
                self._insertdb(result)
            

    def _detectAgeAndGender(self, rect):
        y1, x2, y2, x1 = rect
        imgcrop = self.frame[y1:y2, x1:x2]
        blob = cv2.dnn.blobFromImage(cv2.resize(imgcrop, (256, 256)), 1, (256, 256), 127.5)
        gender_net.setInput(blob)
        age_net.setInput(blob)
        age_prediction = age_net.forward()
        gender_prediction = gender_net.forward()
        
        age = ['0-2th','4-6th','8-12th','15-20th','25-32th','38-43th','48-53th','60-100th'][age_prediction.argmax()]
        gender = ["Laki-laki", "Perempuan"][gender_prediction.argmax()]
        filename = "img/" + str(time.time()) + '.jpg'

        cv2.imwrite(filename, imgcrop)
        return age, gender, filename 


    def _knn(self, item):

        resultEncoding = face_recognition.face_distance(self.dataEncoding, item)
        result = list(zip(resultEncoding, self.dataName))

        max1 = min(result)
        result.remove(max1)

        max2 = min(result)
        result.remove(max2)

        max3 = min(result)
        result.remove(max3)

        max4 = min(result)
        result.remove(max4)

        themin = 0.6

        if max1[0] > themin or max2[0] > themin or max3[0] > themin:
            return None

        if max1[1] == max2[1] and max1[1] == max3[1] and max1[1] == max4[1]:
            return max1[1]

        return None

    def _insertdb(self, name, filename=None):
        c = self.connection.cursor()
        if filename == None:
            c.execute("insert into report (id_camera, type_detection, total, description, time, date) values( 71, 7, 1, '" + name +"', now(), now())")
        else :
            c.execute("insert into report (id_camera, type_detection, total, description, url, time, date) values( 71, 7, 1, '" + name +"', '" + filename + "', now(), now())")

        c.close()
        self.connection.commit()


class DataLoader():

    def load():
        regex = re.compile('\(\d\).JPG.txt')
        dataName = []
        dataEncoding = []
        filepath = './txt/'
        listfiles = os.listdir(filepath)

        for f in listfiles:
            dataName.append(re.sub(regex, '', f))
            dataEncoding.append(numpy.loadtxt(filepath + f))

        return dataName, dataEncoding

def draw(frame):
    r = global_rect
    name = global_name
    color = (0,255,0)

    pt1 = (r[1], r[0])
    pt2 = (r[3], r[2])
    cv2.rectangle(frame, pt1, pt2, color)
    print(r, name)

    ptText = (r[3], r[2] - 10)
    cv2.putText(frame, name, ptText, cv2.FONT_HERSHEY_SIMPLEX, 0.5, color)
    return frame


def main(inputCam):

    global global_name
    global global_rect

    cap = cv2.VideoCapture(inputCam)
    w = cap.get(cv2.CAP_PROP_FRAME_WIDTH);
    h = cap.get(cv2.CAP_PROP_FRAME_HEIGHT); 
    fourcc= cv2.VideoWriter_fourcc(*'x264')
    out = cv2.VideoWriter("index.m3u8", fourcc, 15, (int(w), int(h)))

    #con = mysql.connector.connect(user="root", password="bersaudara", database="bpbd_jkt")
    con = mysql.connector.connect(user="root", password="", database="bpbd_jkt")
    #con = mysql.connector.connect(user="egon", password="nakal23baik", database="bpbd_jakarta")

    names, encs = DataLoader.load()


    keyinput = 0

    fr = FaceRecognizer(con, names, encs, [])

    while keyinput != 27:
        ok, frame = cap.read()

        if not fr.is_alive():
            fr = FaceRecognizer(con, names, encs, frame)
            fr.start()

        fr.join(0.0001)

        keyinput = cv2.waitKey(1)
        frame = draw(frame)

        if global_gui:
            cv2.imshow("w", frame)
        if global_stream:
            out.write(frame)

    cap.release()
    out.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Kurang inputnya")

    if len(sys.argv) == 3:
       if sys.argv[2] == "gs": 
           global_gui = True
           global_stream = True
       if sys.argv[2] == "g": 
           global_gui = True
       if sys.argv[2] == "s": 
           global_stream = True

    inputCam = sys.argv[1]

    if inputCam.isdigit():
        inputCam = int(inputCam)

    main(inputCam)
