#include <my_global.h>
#include "db.h"

DB::DB() {

}

DB::~DB() {
  //std::cout << "~DB()" << std::endl;
}


bool DB::init(
    std::string host, 
    std::string username, 
    std::string password, 
    std::string database
) 

{  
  con = mysql_init(NULL);
  char query[255];

  if (con == NULL) {
    puts("Cannot create connection\n");
    return false;
  }

  if (mysql_real_connect(con, host.c_str(), username.c_str(), password.c_str(), NULL, 0, NULL, 0) == NULL) {
    puts("Cannot connect to\n");
    mysql_close(con);
    return false;
  }

  sprintf(query, "USE %s", database.c_str());

  if (mysql_query(con, query)) {
    std::cout << mysql_error(con) << std::endl;
    return false;
  }
}

bool DB::insert(std::string value, std::string imageFilename) {
  char query[255];
  sprintf(query, "INSERT INTO report(id_camera, type_detection, time, date, total, url, description) VALUES (31, 3, now(), now(), 1, '%s', '%s')", imageFilename.c_str(), value.c_str());

  if (mysql_query(con, query)) {
    std::cout << mysql_error(con) << std::endl;
    return false;
  } 
  return true;
}


