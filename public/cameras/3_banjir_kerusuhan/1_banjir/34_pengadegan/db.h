#include <mysql.h>
#include <stdio.h>
#include <iostream>

class DB {
  private:
    MYSQL *con;
    std::string table;


  public:
    DB();
    ~DB();

    bool init(
        std::string host, 
        std::string username, 
        std::string password, 
        std::string database
        );

    bool insert(std::string value, std::string filename);
};
