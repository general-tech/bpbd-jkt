#include <opencv2/opencv.hpp>
#include <boost/thread.hpp>
#include "db.h"

std::vector<cv::Rect> vDetected;
cv::HOGDescriptor hog;
cv::Mat img;
int frameCount = 0;
DB db;

void draw_locations(cv::Mat & img, const std::vector< cv::Rect > & locations, const cv::Scalar & color)
{
	if (!locations.empty())
	{
		std::vector< cv::Rect >::const_iterator loc = locations.begin();
		std::vector< cv::Rect >::const_iterator end = locations.end();
		for (; loc != end; ++loc)
		{
			cv::rectangle(img, *loc, color, 2);
		}
	}
}

void draw_people_count(cv::Mat &img, const std::vector< cv::Rect > & locations, int frameCount ) {
	int people_count = locations.size();

	/*
	   if (people_count > 0  && frameCount % 6 > 3) {
	   return;
	   }
	 */
	cv::Scalar s = cv::Scalar(255,255,255);

	if (people_count > 1) {
		s = cv::Scalar(0,0,255);
	}

	std::ostringstream str;

	str << "Terdeteksi : " << people_count;

	cv::putText(img, str.str(), cv::Point(0,50), cv::FONT_HERSHEY_PLAIN, 1.01, s, 1, 0);

}

void detect() {
	hog.detectMultiScale(img, vDetected, 0, cv::Size(8, 8), cv::Size(32, 32), 1.05, 2);
	frameCount++;
	if (vDetected.size() > 1) {
		char filename[128];
		sprintf(filename, "img/objek_mayat-%d.jpg", frameCount);
		cv::imwrite(filename, img);
		db.insert((float) vDetected.size(), filename);
	}
	boost::this_thread::yield();
}

int main(int argc, char* argv[]) {

	if (argc < 2) {
		std::cout << "./a.out video mask.jpg" << std::endl;
		return -1;
	}

	db.init("127.0.0.1", "root", "bersaudara", "bpbd_jkt", "report", "total", argv[1]);


	cv::VideoCapture cap(argv[1]);
	cv::Mat img_input;
	cv::Mat mask = cv::imread(argv[2]);
	int peopleCount = 0;

	static std::vector<float> detector = cv::HOGDescriptor::getDefaultPeopleDetector();
	hog.setSVMDetector(detector);

	int frameCountSync = frameCount;
	boost::thread *t;

	while(cv::waitKey(1) != 27) {

		if (!cap.isOpened()) {
			cap.open(argv[1]);
		}

		cap >> img_input;

		try {
			cv::resize(img_input, img_input, cv::Size(960, 540));
		} catch(cv::Exception e) {
			// ok just die and make new connection
			break;
		}
		img_input.copyTo(img, mask);


		if (frameCountSync == frameCount) {
			frameCountSync++;
			if (frameCount > 0)  {
				t->join();
				delete t;
			}
			t = new boost::thread(detect);
		}

		draw_locations(img_input, vDetected, cv::Scalar(0, 255, 0));
		draw_people_count(img_input, vDetected, frameCount);

		cv::imshow("w", img_input);
		//std::cout.write((char *) img_input.data, img_input.total() * img_input.elemSize());
	}
}
