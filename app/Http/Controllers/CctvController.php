<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Camera;
use App\CamerasDetections;

class CctvController extends Controller
{
  
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){  
        $data['listMap'] = DB::table('cameras')->limit(3)->get();
        
        $data['dir_sampah'] = '/cameras/';
        // $data['cctv_sampah'] = DB::table('cameras_detections')->where('type_detection', 1)->get();
        $data['cctv_sampah'] = DB::table('cameras_detections')
        ->join('type_detections', 'cameras_detections.type_detection', '=', 'type_detections.id_detection')
        // ->limit(6)
        ->get();

        return view('cctv.index', $data);
    }


    public function detail($detail){
        // $data['cameras'] = Camera::where('id', $detail)->first();
		$data['cameras'] = DB::table('cameras_detections')
        ->where('id_camera', $detail)
        ->join('type_detections', 'cameras_detections.type_detection', '=', 'type_detections.id_detection')->first();

        if ($data['cameras']) {
            return view('cctv.detail', $data);
        }else{
            return abort(404);;
        }

    }


    public function datajson(Request $request)
    {   
        $Report = DB::table('report')
        ->select(DB::raw('report.id, report.id_camera, report.date, report.valid, time, SUM(total) as total, report.description, report.url, dir'))
        ->join('cameras_detections', 'report.id_camera', '=', 'cameras_detections.id_camera')
        ->where('report.id_camera', $request->id)
        ->groupBy('date', 'id_camera', 'description');

        $dataReport = array();
        $no = 1;
        foreach ($Report->get() as $key) {
            if ($key->valid == null) {
                $valid = '<div class="btn_valid_'.$key->id.'"><a href="javascript:;" class="btn btn-primary" onclick="valid(1, '.$key->id.')"><i class="fa fa-check"></i></a> &nbsp; 
                <a href="javascript:;" class="btn btn-danger" onclick="valid(0, '.$key->id.')"><i class="fa fa-exclamation-circle"></i></a></div>';
            }else if ($key->valid == 'y') {
                $valid = 'Valid';
            }else if ($key->valid == 'n') {
                $valid = 'Tidak Valid';
            }

            $data= array(
                $key->id,
                date('d M Y', strtotime($key->date)),
                $key->time.' WIB',
                number_format($key->total, 3),
                $key->description,
                "<a href='/cameras/".$key->dir."/".$key->url."' download><img src='/cameras/".$key->dir."/".$key->url."' width='100%'></a>",
                $valid
            );
            array_push($dataReport, $data);
            $no++;
        }
        return response()->json(array('data' => $dataReport));
    }

    public function validasi(Request $request){ 
        $reports = DB::table('report')->where('id', $request->id);
        if ($request->indicator == 1 && $reports->count() > 0) {
            $reports->update(['valid' => 'y']);
            return response()->json(array(
                'type' => 'Valid',
                'status' => true
            ));
        }  else if ($request->indicator == 0 && $reports->count() > 0) {
            $reports->update(['valid' => 'n']);
            return response()->json(array(
                'type' => 'Tidak Valid',
                'status' => true
            ));

        } else {
            return response()->json(array(
                'status' => false
            ));

        }
      
    }


}
