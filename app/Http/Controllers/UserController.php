<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
  
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {  
        $users = User::paginate(10);

        return view('setting.user.index', compact('users'));
    }

    public function datajson()
    {
        $users = new User;
        $dataUser = array();
        $no = 1;
        foreach ($users->get() as $key) {
            $data= array(
                $no,
                $key->name,
                $key->email,
                $key->role,
                '
                <a href="/setting/user/edit/'.$key->id.'" class="btn btn-primary"> <i class="fa fa-edit"></i> </a>
                <a href="/setting/user/delete/'.$key->id.'" class="btn btn-danger"> <i class="fa fa-trash"></i> </a>
                '
            );
            array_push($dataUser, $data);
            $no++;
        }
        return response()->json(array('data' => $dataUser));
    }


    public function create()
    {   
        if (Auth::user()->role == 'Admin') {
    	   return view('setting.user.create');
        } else {
            return redirect('setting/user');
        }
    }

    public function store(Request $request)
    {
    	$users = new User;

    	$users->name = $request->name;
        $users->email = $request->email;
        $users->password = bcrypt($request->password);
    	$users->role = $request->role;
    	
    	// $camera->date = date('Y-m-d H:i:s');
    	$users->save();

    	return redirect('setting/user');
    }

    public function edit($id)
    {
    	$users = User::find($id);
        if (Auth::user()->role == 'Admin') {
           return view('setting.user.edit', compact('users'));
        } else {
            return redirect('setting/user');
        }
    }

    public function update(Request $request)
    {
    	$users = User::find($request->id);

    	$users->name = $request->name;
        $users->email = $request->email;
    	$users->role = $request->role;

    	$users->save();

    	return redirect('setting/user');
    }

    public function delete($id)
    {
    	$users = User::find($id);

    	$users->delete();

    	return redirect('setting/user');
    }
}
