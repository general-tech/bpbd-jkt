<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Camera;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
  
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){  
        $data['title'] = 'Setting';

        return view('setting.layout', $data);
    }

    public function indexCamera()
    {
        $data['title'] = 'Pengaturan Camera';
        return view('setting.camera.index', $data);
    }
    
    public function datajson()
    {
    	$camera = new Camera;
        $dataCamera = array();
        $no = 1;
        foreach ($camera->get() as $key) {
            $data= array(
                $no,
                $key->kota,
                $key->kecamatan,
                $key->kelurahan,
                substr($key->url,0 , 50),
                $key->date,
                '
                <a href="/setting/camera/edit/'.$key->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Ubah"> <i class="fa fa-edit"></i> </a>
                <a href="/setting/camera/delete/'.$key->id.'" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus"> <i class="fa fa-trash"></i> </a>
                <a href="'.$key->url.'" target="_blank" class="btn btn-orange" data-toggle="tooltip" data-placement="top" title="Lihat"> <i class="fa fa-eye"></i> </a>
                '
            );
            array_push($dataCamera, $data);
            $no++;
        }
        return response()->json(array('data' => $dataCamera))->header('Content-Type', 'application/json');
    }
    


    public function createCamera()
    {   
        if (Auth::user()->role == 'Admin') {
    	   return view('setting.camera.create');
        } else {
            return redirect('setting/camera');
        }
    }

    public function storeCamera(Request $request)
    {
    	$camera = new Camera;

    	$camera->id_camera = $request->id_camera;
    	$camera->kota = $request->id_camera;
    	$camera->kecamatan = $request->id_camera;
    	$camera->kelurahan = $request->id_camera;
    	$camera->lat = $request->id_camera;
    	$camera->lng = $request->id_camera;
    	$camera->url = $request->id_camera;
    	$camera->date = date('Y-m-d H:i:s');

    	$camera->save();

    	return redirect('setting/camera');
    }

    public function editCamera($id)
    {
    	$camera = Camera::find($id);
        if (Auth::user()->role == 'Admin') {
           return view('setting.camera.edit', compact('camera'));
        } else {
            return redirect('setting/camera');
        }
    	
    }

    public function updateCamera(Request $request)
    {
    	$camera = Camera::find($request->id);

    	$camera->id_camera = $request->id_camera;
    	$camera->kota = $request->kota;
    	$camera->kecamatan = $request->kecamatan;
    	$camera->kelurahan = $request->kelurahan;
    	$camera->lat = $request->lat;
    	$camera->lng = $request->lng;
    	$camera->url = $request->url;
    	$camera->date = date('Y-m-d H:i:s');

    	$camera->save();

    	return redirect('setting/camera');
    }

    public function deleteCamera($id)
    {
    	$camera = Camera::find($id);

    	$camera->delete();

    	return redirect('setting/camera');
    }
}
