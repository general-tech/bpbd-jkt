<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller{
    public function __construct(){
        if(Auth::check()) {
            return redirect('/');
        }
    }

  	public function index(){
      	$data['title'] = 'User';
        $userdata = array(
          'email' => 'rizky@gmail.com',
        );
        if (Auth::attempt($userdata)) {
          return redirect()->intended('/');
        }else{
          echo Auth::check();
        }

	    // return view('auth.login', $data);
  	}

 

    public function input(Request $request){
      $validator = Validator::make($request->all(), [
          'email'    => 'required|email',
          'password' => 'required|alphaNum|min:3'
      ]);
      if ($validator->fails()) {

          return Redirect::back()->withErrors($validator);
      }else{
    
          $userdata = array(
            'email' => $request->email,
            'password' => $request->password,
          );
          if (Auth::attempt($userdata)) {
            return redirect()->intended('/');
          }else{
            return Redirect::back()->withErrors([
               'Email atau Password anda salah!'
            ]);
          }


      }	
  	}




    


}