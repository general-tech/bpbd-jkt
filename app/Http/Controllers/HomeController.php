<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
  
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){  
        $data['title'] = 'Home';
		$data['listMap'] = DB::table('cameras')->limit(250)->get();
		
		// $circle_radius = 603502;
		// $max_distance = 20;
		// $lat = -6.309320;
		// $lng = 106.912743;

		// $data['marker'] = DB::select('
		// 	SELECT *, ( '.$circle_radius.' * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) 
		// 	* cos( radians( lng ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin(radians(lat)) ) ) AS distance
		// 	FROM cameras
		// 	ORDER BY distance
		// 	LIMIT 0, 20;
		// ');
		



        return view('home.index', $data);
    }

    
    public function listmap(Request $request){  
    	// $data['listMap'] = DB::table('cameras')->limit(250)->get();
    	// return json_decode($data['listMap']);
		
		$circle_radius = 50;
		$lat = $request->lat;
		$lng = $request->lng;

		// (100/6378.1) * 6378100


		$data['marker'] = DB::select('
			SELECT *,
			ACOS( SIN( RADIANS( `lat` ) ) * SIN( RADIANS( '.$lat.' ) ) + COS( RADIANS( `lat` ) )
			* COS( RADIANS( '.$lat.' )) * COS( RADIANS( `lng` ) - RADIANS( '.$lng.'
			 )) ) * 50000 AS `distance`
			FROM `cameras`
			WHERE
			ACOS( SIN( RADIANS( `lat` ) ) * SIN( RADIANS( '.$lat.' ) ) + COS( RADIANS( `lat` ) )
			* COS( RADIANS( '.$lat.' )) * COS( RADIANS( `lng` ) - RADIANS( '.$lng.'
			 )) ) * 50000 < 10
			ORDER BY `distance`
		');

  //   	$data['marker'] = DB::select('
		// 	SELECT *, ( '.$circle_radius.' * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) 
		// 	* cos( radians( lng ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin(radians(lat)) ) ) AS distance
		// 	FROM cameras
		// 	ORDER BY distance
		// 	LIMIT 0, 20;
		// ');
    	return response()->json($data['marker'], 200);

        
    }


}
