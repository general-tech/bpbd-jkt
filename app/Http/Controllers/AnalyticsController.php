<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\CamerasDetection;
use App\TypeDetection;

class AnalyticsController extends Controller
{
  
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){  
      $cameras_d = new CamerasDetection;
      $data['getList'] = TypeDetection::all();

      return view('analytics.index', $data);
        
    }

    public function indexs_delete(Request $request){  
        
        $cameras_d = new CamerasDetection;
        $data['getList'] = TypeDetection::get();
        $listParams = array(
            'type',
            't_d',
            's_date',
            'e_date',
        );
        
        if ($request->has($listParams)) {
            
            if ($request->t_d == 'jam') {
                $getData = DB::table('report')
                ->select(DB::raw('id, date, type_detection, type, SUM(total) AS total, token_detection, time '))
                ->join('type_detections', 'report.type_detection', '=', 'type_detections.id_detection')
                ->where('type_detections.token_detection', $request->type)
                ->where('date', date('Y-m-d'))
                ->whereBetween('time', [$request->s_time.":00", $request->e_time.":00"])
                ->groupBy('time')
                ->get();

            } else  {
                $getData = DB::table('report')
                ->select(DB::raw('id, date, type_detection, type, SUM(total) AS total, token_detection '))
                ->join('type_detections', 'report.type_detection', '=', 'type_detections.id_detection')
                ->where('type_detections.token_detection', $request->type)
                ->whereBetween('date', [$request->s_date, $request->e_date])
                ->groupBy('date')
                ->get();
                
            }
            if ($getData->first() != null) {
                $data['status'] = 200;
                $c_data = array();
                $c_date = array();
                // $data['type'] = $getData->first()->type_detection;
                foreach ($getData as $vdata) {
                    array_push($c_data, $vdata->total);
                    if ($request->t_d == 'jam') {
                        array_push($c_date, $vdata->time);
                    } else {
                        array_push($c_date, date('d M Y', strtotime($vdata->date)));
                    }
                }
                $data['type_detection'] = $getData->first()->type;

                $data['chartjs'] = app()->chartjs
                ->name('lineChartTest')
                ->type('line')
                ->labels($c_date)
                ->datasets([
                    [
                        "label" => $data['type_detection'],
                        'backgroundColor' => "rgba(245, 127, 3, 0.31)",
                        'borderColor' => "rgba(245, 127, 3, 0.7)",
                        "pointBorderColor" => "rgba(245, 127, 3, 0.7)",
                        "pointBackgroundColor" => "rgba(245, 127, 3, 0.7)",
                        "pointHoverBackgroundColor" => "#fff",
                        "pointHoverBorderColor" => "rgba(220,220,220,1)",
                        'data' => $c_data,
                    ],
                ])
                ->options([]);


            } else {
                $data['status'] = 404;

            }
            
            return view('analytics.index', $data);
        } else {
            $data['status'] = 204;
            return view('analytics.index', $data);
        } 
    }
  
    public function dataChart_hari(Request $request)
    {
       $listParams = array('type', 's_date', 'e_date');
        if ($request->has($listParams)) {
          $label = TypeDetection::where('token_detection', $request->type)->first();
          
          $getData = DB::table('report')
          ->select(DB::raw('report.id, report.date, report.type_detection, report.id_camera, type, SUM(total) AS total, token_detection, color '))
          ->join('type_detections', 'report.type_detection', '=', 'type_detections.id_detection')
          ->join('cameras_detections', 'report.id_camera', '=', 'cameras_detections.id_camera')
          
          ->where('type_detections.token_detection', $request->type)
          ->whereBetween('report.date', [$request->s_date, $request->e_date])
          ->groupBy('date', 'id_camera')
          ->orderBy('id', 'asc')
          ->get();


          $c_date = [];
          $dataAnalytic = [];
          foreach ($getData as $vdata) {
              $key = $vdata->id_camera;
              $dataAnalytic[$key][] = [
                'c_data' => number_format($vdata->total, 2), 
                'label' => $vdata->id_camera,
                'color' => $vdata->color,
              ];
              array_push($c_date, date('d M Y', strtotime($vdata->date)));
          }
          
          return response()->json([
            'status' => 'true', 
            'dataAnalytic' => $dataAnalytic,
            
            'c_date' => array_values(array_unique($c_date)), 
            'label' => $label,
          ]);

        }
    }
    
    public function dataChart_jam(Request $request)
    {
        $listParams = array('type', 's_date', 's_time', 's_time');
        if ($request->has($listParams)) {
          $label = TypeDetection::where('token_detection', $request->type)->first();

          $getData = DB::select('select report.id, report.time, report.type_detection, report.id_camera, type, SUM(total) AS total, token_detection, updated_at, color from report
            inner join type_detections on report.type_detection = type_detections.id_detection
            inner join cameras_detections on report.id_camera = cameras_detections.id_camera
            where type_detections.token_detection = "'.$request->type.'" AND
            report.date = "'.$request->s_date.'" AND
            report.time BETWEEN "'.$request->s_time.':00" AND "'.$request->e_time.':00"
            GROUP BY hour(time), id_camera
            order by id asc');

          $c_data = [];
          $c_date = [];
          $dataAnalytic = [];
          foreach ($getData as $vdata) {
              $key = $vdata->id_camera;
              $dataAnalytic[$key][] = [
                'c_data' => number_format($vdata->total, 2), 
                'label' => $vdata->id_camera,
                'color' => $vdata->color,
              ];
              array_push($c_date, $vdata->time. ' WIB');
          }

          return response()->json([
            'status' => 'true', 
            'dataAnalytic' => $dataAnalytic,

            // 'c_data' => $c_data, 
            'c_date' => array_values(array_unique($c_date)), 
            'label' => $label,
          ]);
        }
    }






}
