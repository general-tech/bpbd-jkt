<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotifController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {  

        $datas = DB::select('SELECT * FROM report WHERE time BETWEEN timestamp(DATE_SUB(NOW(), INTERVAL 20 second)) AND timestamp(NOW()) AND total > 25 AND id_camera in ("41", "42", "43")');

        $data_array = array();
        foreach ($datas as $data) {
            array_push($data_array, $data);
        }
      
        if ($data_array != null) {
            return response()->json(array(
                'type' => 'Kerusuhan',
                'status' => true
            ));
        }

        // return response()->json(array(
        //         'type' => 'Kerusuhan',
        //         'status' => true
        //     ));

    }

    public function all()
    {
        $data['title'] = 'Notifikasi';
        $data['getData'] = DB::table('report')->join('type_detections', 'report.type_detection', '=', 'type_detections.id_detection')->orderBy('id',' desc')->paginate(20);
        return view('notifikasi.index', $data)->with('no', 1);
    }


}
